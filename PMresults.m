%Matlab applications in physics
%Author: Izabela Moraczewska
%Technical Physics

%pliki is list of files with data 
%use your path for file

pliki(1)=("C:\Users\hp\Desktop\data\dane-pomiarowe_2019-10-08")
pliki(2)=("C:\Users\hp\Desktop\data\dane-pomiarowe_2019-10-09")
pliki(3)=("C:\Users\hp\Desktop\data\dane-pomiarowe_2019-10-10")
pliki(4)=("C:\Users\hp\Desktop\data\dane-pomiarowe_2019-10-11")
pliki(5)=("C:\Users\hp\Desktop\data\dane-pomiarowe_2019-10-12")
pliki(6)=("C:\Users\hp\Desktop\data\dane-pomiarowe_2019-10-13")
pliki(7)=("C:\Users\hp\Desktop\data\dane-pomiarowe_2019-10-14")


maksimum = 0;
srednie = zeros(3,7);


for i = 1:7
    suma1=0
    suma2=0
    suma3=0
    %thanks Oskar Klos for idea for use of this function
    p1 = pliki(i);
    opcje = detectImportOptions(p1);
    opcje.DataLines = [2,25];
    table = readtable(p1,opcje);
    temp = table2array(table(:,6:6));
    

    for j=1:24 
        % calculate maximum PM10 
       if maksimum < temp(j)
          maksimum = temp(j);
          time = j;
          day = i+7; 
       end
       % calculate average pm10 for periods of time (dusk and dawn) 
       if j<8
       suma1 = suma1 + temp(j)
       end
       if j>7 || j<18
       suma2 = suma2 + temp(j)
       end
       if j>17
       suma3 = suma3 + temp(j)
       end
    end  
    
    srednie(1,i) = suma1
    srednie(2,i) = suma2
    srednie(3,i) = suma3
end

%periods of time for night 1:00 - 7:00 ; 17:00 - 24:00
%period of time for day 8:00 - 16:00 ; 
noc = zeros(1,8);
noc(1) = srednie(1,1)/7;
noc(8) = srednie(3,3)/8;
dzien = zeros(1,7)

%computing averages for each period
for i = 1:6
    noc(i+1) = srednie(3,i)/7 + srednie(1,i+1)/8
end
for i = 1:7
    dzien(i) = srednie(2,i)/9
end

dni = [7,8,9,10,11,12,13,14,15];%array for printing


%Printing outcomes
fprintf('Analysed time: \nfrom: 08-10-2019, 00:00 to 14-10-2019, 24:00\n');
fprintf('Max concentration: %d PM10 during %d-10-2019 at %d:00\n\n',maksimum,day,time);

for i = 1:7
    fprintf('Average concenctration for days: %.3f date - %d-10-2019\n',dzien(i),dni(i+1));
end

for i = 1:8
    fprintf('Average concenctration for nights: %.f between %.f and %.3f 10-2019\n',noc(i),dni(i),dni(i+1));
end

%writing the same text into a file
wynik = ('PMresult.dat');
f1 = fopen(wynik,'w');

fprintf(f1,'Analysed time: \nfrom: 08-10-2019, 00:00 to 14-10-2019, 24:00\n');
fprintf(f1,'Max concentration: %d PM10 during %d-10-2019 at %d:00\n\n',maksimum,day,time);

for i = 1:7
    fprintf(f1,'Average concenctration for days: %.3f date - %d-10-2019\n',dzien(i),dni(i+1));
end

for i = 1:8
    fprintf(f1,'Average concenctration for nights: %.f between %.f and %.3f 10-2019\n',noc(i),dni(i),dni(i+1));
end
fclose(f1);